package com.example.back.domain.game.events;

import com.example.back.domain.game.Player;

public class RespondentEvent extends Event{
    public final Player player;
    public RespondentEvent(Player player) {
        super("RespondentEvent");
        this.player = player;
    }
}
