package com.example.back.domain.game;

import com.example.back.domain.User;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
public class Player implements Serializable {
    User user;
    int score;

    public Player(User user) {
        this.user = user;
    }
}
