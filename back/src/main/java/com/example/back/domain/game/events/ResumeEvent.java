package com.example.back.domain.game.events;

public class ResumeEvent extends Event{
    public ResumeEvent() {
        super("ResumeEvent");
    }
}
