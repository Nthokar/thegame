package com.example.back.domain.game;

import com.example.back.app.services.exceptions.PlayerNotFoundException;
import com.example.back.domain.game.pack.Pack;
import com.example.back.domain.game.pack.Round;
import com.example.back.domain.game.pack.lot.Lot;
import com.example.back.domain.game.packsecured.SecuredRound;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@RedisHash("Lobby")
public class Lobby implements Serializable {
    @Id
    @Getter
    String id;

    @Getter
    Pack pack;

    @Getter
    GameState gameState;

    @Getter
    LobbyState lobbyState;

    @Getter
    Master master;

    @JsonIgnore
    private LocalDateTime pingPongStart;

    @JsonIgnore
    private LocalDateTime pingPongEnd;

    @JsonIgnore
    private Integer currentRoundIndex;

    @Getter
    @JsonIgnore
    private Player picker;

    @JsonIgnore
    @Getter
    private Player respondent;

    @JsonIgnore
    @Getter
    private Lot currentLot;

    @Getter
    public Round currentRound;

    @Getter
    @Setter
    private List<Player> players;

    public Lobby(Master master) {
        this.players = new ArrayList<>();
        this.master = master;
        this.id = UUID.randomUUID().toString();
        this.lobbyState = LobbyState.NOT_STARTED;
    }
    public void setPicker(String userId) {
        picker = findPlayerByID(userId);
    }
    public void setPack(Pack pack) {
        if (lobbyState.equals(LobbyState.NOT_STARTED)) {
            this.pack = pack;
        }
    }

    public Map<String, Lot[]> currentRoundMap() {
        return pack.getRounds().get(currentRoundIndex).asMap();
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void startLobby() {
        if (Objects.nonNull(master) && !players.isEmpty() && Objects.nonNull(pack) && lobbyState.equals(LobbyState.NOT_STARTED)) {
            lobbyState = LobbyState.GOING;
            currentRoundIndex = 0;
            currentRound = pack.getRounds().get(currentRoundIndex);
            Optional<Player> player = players.stream().filter(Objects::nonNull).findFirst();
            if (player.isPresent()) {
                picker = player.get();
            }
        }
        else {
            //TODO
            //Надо добавить системку экзепшенов, чтобы в описание дописывалась причина по которой лобак не стартует.
            throw new RuntimeException("lobby couldn't be started");
        }
    }
    public Round getSecuredCurrentRound(){
        return currentRound;
    }

    public boolean tryPauseGame(){
        if (lobbyState.equals(LobbyState.GOING)) {
            lobbyState = LobbyState.PAUSED;
            return true;
        }
        return false;
    }

    public boolean tryResumeGame(){
        if (lobbyState.equals(LobbyState.PAUSED)) {
            lobbyState = LobbyState.GOING;
            return true;
        }
        return false;
    }

    public void onLotChoose(LotDTO lot) {
        currentLot = currentRound.getLot(lot.getCategoryName(), lot.getIndex());
        //set ping pong start
        startPingPong(2l);

        currentRound.removeLot(lot.getCategoryName(), lot.getIndex());
    }

    public Player onCorrectAnswer() {
        changePlayerScore(respondent, currentLot.getReward());
        picker = respondent;
        return respondent;
    }

    public Player onWrongAnswer() {
        changePlayerScore(respondent, -currentLot.getReward());
        startPingPong(0l);
        return respondent;
    }

    public Player changePlayerScore(String userId, int delta) {
        Player player = findPlayerByID(userId);
        player.score += delta;
        return player;
    }

    public void updateRound() {
        if (currentRoundIndex + 1 >= pack.getRounds().size())
            endGame();
        else {
            currentRoundIndex += 1;
            currentRound = pack.getRounds().get(currentRoundIndex);
        }
    }

    public Map<String, Lot[]> getAvailableLots() {
        return currentRoundMap();
    }

    /**
     *
     * @param userId
     * @return returns true if user win ping pong run
     */
    public boolean userTry(String userId) {
        LocalDateTime now = LocalDateTime.now();
        if (gameState == GameState.PING_PONG && lobbyState == LobbyState.GOING) {
            if (now.isBefore(pingPongStart)) {
                //наказание
            }
            else {
                respondent = findPlayerByID(userId);
                gameState = GameState.ANSWERING;
            }
            return !now.isBefore(pingPongStart);
        }
        return false;
    }

    public void endGame() {

    }

    public void leave(String userId) {
        Player player = findPlayerByID(userId);
        players.remove(player);
    }
    private void startPingPong(Long delayInSeconds) {
        gameState = GameState.PING_PONG;
        pingPongStart = LocalDateTime.now().plusSeconds(delayInSeconds);
        pingPongEnd = pingPongStart.plusMinutes(1);
    }

    private Player findPlayerByID(String userId) {
        return players
                .stream()
                .filter(x -> x.user.getId().equals(userId))
                .findFirst()
                .orElseThrow(() -> new PlayerNotFoundException(id, userId));
    }
    private void changePlayerScore(Player player, int delta) {
        player.score += delta;
    }
}
