package com.example.back.domain.game.events;
public class Event {
    public final String eventType;

    Event(String eventType) {
        this.eventType = eventType;
    }
}
