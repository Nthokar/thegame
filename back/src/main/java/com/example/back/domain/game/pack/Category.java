package com.example.back.domain.game.pack;

import com.example.back.domain.game.pack.lot.Lot;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.redis.core.RedisHash;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Data
public class Category {
    String title;
    List<Lot> lots;

    public Lot getLot(Integer index) {
        return lots.stream().filter(l -> l.getIndex().equals(index))
            .findFirst().orElseThrow();
    }

    public Category(List<Lot> lots) {
        this.lots = lots;
    }
}
