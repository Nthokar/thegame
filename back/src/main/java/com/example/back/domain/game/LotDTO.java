package com.example.back.domain.game;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class LotDTO {

    private String categoryName;
    private int index;

    public LotDTO(String categoryName, int index) {
        this.categoryName=categoryName;
        this.index=index;
    }
}
