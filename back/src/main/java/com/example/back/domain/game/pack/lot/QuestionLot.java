package com.example.back.domain.game.pack.lot;

import com.example.back.domain.game.pack.Question;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.redis.core.RedisHash;

@NoArgsConstructor
@JsonTypeName("QuestionLot")
@ToString(onlyExplicitlyIncluded = true)
@Data
public class QuestionLot extends Lot {
    Question question;

    public QuestionLot(Question question, Integer reward, Integer index) {
        super(reward, QuestionLot.class.getTypeName(), index);
        this.question = question;
    }
}