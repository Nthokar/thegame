package com.example.back.domain.game;

import com.example.back.domain.User;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Data
@NoArgsConstructor
public class Master implements Serializable {
    User user;

    public Master(User user) {
        this.user = user;
    }
}
