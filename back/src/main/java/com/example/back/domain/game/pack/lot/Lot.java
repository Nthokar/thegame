package com.example.back.domain.game.pack.lot;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = QuestionLot.class, name = "QuestionLot"),
})
public abstract class Lot {
    @Getter
    Integer reward;
    @Getter
    Integer index;
    String type;

    Lot(Integer reward, String type, Integer index) {
        this.reward = reward;
        this.type = type;
        this.index = index;
    }
}
