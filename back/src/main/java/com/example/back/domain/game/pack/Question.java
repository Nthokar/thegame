package com.example.back.domain.game.pack;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Data
public class Question {
    Content content;
    String answer;

    public Question(Content content, String answer) {
        this.content = content;
        this.answer = answer;
    }
}