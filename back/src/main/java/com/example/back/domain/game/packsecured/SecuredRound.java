package com.example.back.domain.game.packsecured;


import com.example.back.domain.game.pack.Round;
import com.example.back.domain.game.pack.lot.Lot;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecuredRound {
    Map<String, List<SecuredLot>>  categories;

    public SecuredRound(Map<String, Lot[]> round){
        this.categories=new HashMap<>();
        round.forEach((x,y)-> categories.put(x, Arrays.stream(y).map(SecuredLot::new).toList()));
    }
}