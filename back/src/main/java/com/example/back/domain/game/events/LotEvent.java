package com.example.back.domain.game.events;

import com.example.back.domain.game.pack.lot.Lot;

public class LotEvent extends Event {
    public final Lot lot;
    public LotEvent(Lot lot) {
        super("LotEvent");
        this.lot = lot;
    }
}
