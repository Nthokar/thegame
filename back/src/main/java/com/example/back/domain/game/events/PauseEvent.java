package com.example.back.domain.game.events;

public class PauseEvent extends Event {

    public PauseEvent() {
        super("PauseEvent");
    }
}
