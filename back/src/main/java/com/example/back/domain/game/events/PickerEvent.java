package com.example.back.domain.game.events;

import com.example.back.domain.game.Player;

public class PickerEvent extends Event {
    public final Player player;

    public PickerEvent(Player player) {
        super("PickerEvent");
        this.player = player;
    }
}
