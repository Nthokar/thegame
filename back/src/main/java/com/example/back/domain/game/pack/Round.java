package com.example.back.domain.game.pack;

import com.example.back.domain.game.pack.lot.Lot;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.*;

@NoArgsConstructor
@Document
@Data
public class Round implements Serializable {
    private List<Category> categories;

    public Round(List<Category> categories) {
        this.categories = categories;
    }

    public Map<String, Lot[]> asMap() {
        HashMap<String, Lot[]> map = new HashMap<>();
        for (var category:categories) {
            map.put(category.getTitle(), category.lots.toArray(new Lot[0]));
        }
        return map;
    }

    public Lot getLot(String categoryName, Integer index) {
        return categories.stream().filter(c -> c.title.equals(categoryName))
            .findFirst().orElseThrow()
            .getLot(index);
    }

    public void removeLot(String categoryName, int index) {
        var category = categories.stream().filter(x -> x.title.equals(categoryName)).findFirst()
            .orElseThrow();
        category.lots.removeIf(x -> x.getIndex().equals(index));
    }
}