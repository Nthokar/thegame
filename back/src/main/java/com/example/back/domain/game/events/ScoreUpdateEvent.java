package com.example.back.domain.game.events;

import com.example.back.domain.game.Player;

public class ScoreUpdateEvent extends Event {

    public final Player player;

    public ScoreUpdateEvent(Player player) {
        super("UpdateScoreEvent");
        this.player = player;
    }
}
