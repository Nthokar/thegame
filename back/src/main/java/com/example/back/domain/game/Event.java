package com.example.back.domain.game;

import lombok.Getter;

@Getter
public class Event {
    String sub;
    String lobbyId;
    EventType eventType;

    public Event(EventType eventType, String sub, String lobbyId) {
        this.eventType = eventType;
        this.sub = sub;
        this.lobbyId = lobbyId;
    }
}
