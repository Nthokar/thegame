package com.example.back.domain.game;

public enum LobbyState {
    NOT_STARTED,
    /**
     * Game is paused
     */
    PAUSED,

    GOING,
    /**
     * game ended
     */
    ENDED,
}
