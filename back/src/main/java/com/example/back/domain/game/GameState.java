package com.example.back.domain.game;

public enum GameState {
    LOT_INIT,
    /**
     * player is answering now
     */
    ANSWERING,
    /**
     * players tries to get answer authority
     */
    PING_PONG,
    /**
     * player is choosing next lot
     */
    CHOOSING,

}
