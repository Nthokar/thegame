package com.example.back.domain.game.pack;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Data
@RedisHash
@NoArgsConstructor
public class Pack implements Serializable {
    @Id
    @ToString.Include
    String id;

    List<Round> rounds;

    public Pack(List<Round> rounds) {
        this.id = String.valueOf(UUID.randomUUID());
        this.rounds = rounds;
    }

    public Pack(List<Round> rounds, String id) {
        this.id = id;
        this.rounds = rounds;
    }
}