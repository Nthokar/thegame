package com.example.back.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Data
@NoArgsConstructor
public class User implements Serializable {
    String id;
    String name;

    public User(User user) {
        this.id = user.id;
        this.name = user.name;
    }

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
