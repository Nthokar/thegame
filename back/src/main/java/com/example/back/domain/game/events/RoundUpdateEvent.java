package com.example.back.domain.game.events;

import com.example.back.domain.game.pack.Round;

public class RoundUpdateEvent extends Event {
    public final Round round;
    public RoundUpdateEvent(Round round) {
        super("RoundUpdateEvent");
        this.round = round;
    }
}
