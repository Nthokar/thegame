package com.example.back.domain.game;

public enum EventType {
    ADD_USER,
    LEAVE,
    START,
    PAUSE,
    RESUME,
    ERROR,
    CHANGE_ROUND,
    BID,
    DIO,
    CHOOSE_LOT,
    MASTER_VERIFICATION,
    SCORE_UPDATE,
    ANSWERING,
    CHANGE_PICKER;
}
