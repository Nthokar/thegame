package com.example.back.domain.game.events;

import com.example.back.domain.game.pack.Pack;

public class SetPackEvent extends Event {
    public final Pack pack;
    public SetPackEvent(Pack pack) {
        super("SetPackEvent");
        this.pack = pack;
    }
}
