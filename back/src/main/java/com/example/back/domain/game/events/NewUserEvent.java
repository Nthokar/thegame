package com.example.back.domain.game.events;

import com.example.back.domain.game.Player;

public class NewUserEvent extends Event {
    public final Player player;
    public NewUserEvent(Player player) {
        super("NewUserEvent");
        this.player = player;
    }
}
