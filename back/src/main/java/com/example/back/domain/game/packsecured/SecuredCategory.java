package com.example.back.domain.game.packsecured;

import com.example.back.domain.game.pack.Category;

import java.util.List;

public class SecuredCategory {
    String title;
    List<SecuredLot> lots;

    public SecuredCategory(Category category){
        title=category.getTitle();
        lots=category.getLots().stream().map(SecuredLot::new).toList();
    }
}
