package com.example.back.app.services;

import com.example.back.domain.User;
import com.example.back.app.repos.LobbyRepo;
import com.example.back.app.services.exceptions.LobbyNotFoundException;
import com.example.back.domain.game.Lobby;
import com.example.back.domain.game.LotDTO;
import com.example.back.domain.game.Master;
import com.example.back.domain.game.Player;
import com.example.back.domain.game.events.*;
import com.example.back.domain.game.pack.Pack;
import com.example.back.domain.game.pack.Round;
import com.example.back.domain.game.pack.lot.Lot;
import com.example.back.domain.game.packsecured.SecuredRound;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class LobbyService {
    private final LobbyRepo lobbyRepo;

    public LobbyService(LobbyRepo lobbyRepo) {
        this.lobbyRepo = lobbyRepo;
    }

    public Lobby createLobby(User user) {
        Master master = new Master(user);
        Lobby lobby = new Lobby(master);
        lobbyRepo.save(lobby);
        return lobby;
    }

    public List<Lobby> findAll() {
        return lobbyRepo.findAll();
    }
    public Optional<Lobby> findLobbyById(String lobbyId) {
        return lobbyRepo.findById(lobbyId);
    }
    public Lobby findLobbyByIdOrThrow(String lobbyId) {
        return lobbyRepo.findById(lobbyId).orElseThrow(() -> new LobbyNotFoundException(lobbyId));
    }

    public void addPlayerToLobby(User user, String id) {
        Optional<Lobby> optionalLobby = lobbyRepo.findById(id);
        if (optionalLobby.isPresent()) {
            Lobby lobby = optionalLobby.get();
            lobby.addPlayer(new Player(user));
            lobbyRepo.save(lobby);
        }
    }

    public Event[] pauseGame(String lobbyId) {
        Lobby lobby = findLobby(lobbyId);
        if (lobby.tryPauseGame()) {
            lobbyRepo.save(lobby);
            return new Event[]{new PauseEvent()};
        }
        return null;
    }
    public Event[] resumeGame(String lobbyId) {
        Lobby lobby = findLobby(lobbyId);
        if (lobby.tryResumeGame()) {
            lobbyRepo.save(lobby);
            return new Event[]{new PauseEvent()};
        }
        return null;
    }
    public Event[] start(String lobbyId){
        Lobby lobby = findLobby(lobbyId);
        lobby.startLobby();
        lobbyRepo.save(lobby);
        return new Event[] {new StartEvent(), new PickerEvent(lobby.getPicker())};
    }
    public Player addUser(String lobbyId, User user) {
        //  |  это че, ну я знаю что это, но хз
        // \ /
        //  v


        Lobby lobby = findLobby(lobbyId);
        Player player=new Player(user);
        lobby.addPlayer(player);
        lobbyRepo.save(lobby);
        return player;
    }
    public Player leaveUser() {
        return null;
    }
    public Event[] answerVerification(String lobbyId, boolean isCorrect) {
        Lobby lobby = findLobby(lobbyId);
        if (isCorrect) {
            Player player = lobby.onCorrectAnswer();
            lobbyRepo.save(lobby);
            return new com.example.back.domain.game.events.Event[] {
                    new ScoreUpdateEvent(player),
                    new PickerEvent(player)
            };
        }
        else {
            Player player = lobby.onWrongAnswer();
            lobbyRepo.save(lobby);
            return new com.example.back.domain.game.events.Event[] {
                new ScoreUpdateEvent(player),
                new LotEvent(lobby.getCurrentLot())
            };
        }
    }

    public Event[] scoreUpdate(String lobbyId, String targetId, int delta) {
        Lobby lobby = findLobby(lobbyId);
        Player player = lobby.changePlayerScore(targetId, delta);
        lobbyRepo.save(lobby);
        return new com.example.back.domain.game.events.Event[] {new ScoreUpdateEvent(player)};
    }

    public Event[] tryPingPong(String lobbyId, String userId) {
        Lobby lobby = findLobby(lobbyId);
        if (lobby.userTry(userId)) {
            lobbyRepo.save(lobby);
            return new com.example.back.domain.game.events.Event[]{ new RespondentEvent(lobby.getRespondent())};
        }
        return null;
    }

    public Round getCurRound(String lobbyId){
        return findLobby(lobbyId).getSecuredCurrentRound();
    }

    public Player setPicker(String lobbyId, String userId) {
        Lobby lobby = findLobby(lobbyId);
        lobby.setPicker(userId);
        lobbyRepo.save(lobby);
        return lobby.getPicker();
    }

    public void setPack(String lobbyId, Pack pack) {
        Lobby lobby = findLobby(lobbyId);
        lobby.setPack(pack);
        lobbyRepo.save(lobby);
    }

    public Event[] pickLot(String lobbyId, LotDTO lotDTO) {
        Lobby lobby = findLobby(lobbyId);
        lobby.onLotChoose(lotDTO);
        lobbyRepo.save(lobby);
        return new Event[] {
            new LotEvent(lobby.getCurrentLot()),
            new RoundUpdateEvent(lobby.currentRound)
        };
    }

   /* public SecuredLot getSecuredLot(String lobbyId, int categoryIndex,int questionIndex) {

    }
    public String getAnswer(){

    }*/

    private Lobby findLobby(String lobbyId) {
        return lobbyRepo.findById(lobbyId)
                .orElseThrow(()-> new LobbyNotFoundException(lobbyId));
    }
}
