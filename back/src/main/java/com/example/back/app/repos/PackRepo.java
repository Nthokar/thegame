package com.example.back.app.repos;

import com.example.back.domain.game.pack.Pack;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PackRepo extends CrudRepository<Pack, String> {
}
