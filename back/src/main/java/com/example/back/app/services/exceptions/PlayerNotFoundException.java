package com.example.back.app.services.exceptions;

public class PlayerNotFoundException extends RuntimeException {

    /**
     *
     * @param lobbyId lobby where tried to find player
     * @param userId user which trying to find
     */
    public PlayerNotFoundException(String lobbyId, String userId) {
        super(String.format("No player with id: %s in lobby with id %s", lobbyId, userId));
    }}
