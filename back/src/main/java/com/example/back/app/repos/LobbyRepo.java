package com.example.back.app.repos;

import com.example.back.domain.game.Lobby;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LobbyRepo extends CrudRepository<Lobby, String> {
    List<Lobby> findAll();
}
