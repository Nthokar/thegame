package com.example.back.app.services;

import com.example.back.app.repos.PackRepo;
import com.example.back.domain.game.pack.Pack;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PackService {
    private final PackRepo packRepo;

    public PackService(PackRepo packRepo) {
        this.packRepo = packRepo;
    }

    public void createPack(Pack pack) {
        packRepo.save(pack);
    }

    public Optional<Pack> findPackById(String packId) {
        return packRepo.findById(packId);
    }
}
