package com.example.back.app.services.exceptions;

public class LobbyNotFoundException extends RuntimeException {
    /**
     *
     * @param lobbyId id of lobby which couldn't be found
     */
    public LobbyNotFoundException(String lobbyId) {
        super(String.format("No lobby with id: %s", lobbyId));
    }
}
