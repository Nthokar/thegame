package com.example.back.infrastructure.controllers;

import com.example.back.domain.game.Event;
import com.example.back.domain.game.EventType;
import com.example.back.app.services.LobbyService;
import com.example.back.domain.User;
import com.example.back.domain.game.Lobby;
import com.example.back.domain.game.pack.Pack;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/lobby")
public class LobbyController {
    private final LobbyService lobbyService;
    private final SimpMessagingTemplate messagingTemplate;

    public LobbyController(LobbyService lobbyService,SimpMessagingTemplate messagingTemplate) {
        this.lobbyService = lobbyService;
        this.messagingTemplate=messagingTemplate;
    }

    @PostMapping("/create")
    public LobbyDTO createGame(@RequestBody User user) {
        return new LobbyDTO(lobbyService.createLobby(user));
    }

    @PutMapping("/connect")
    public void connectToLobby(@RequestBody User user, @RequestParam String lobbyId) {
        lobbyService.addPlayerToLobby(user, lobbyId);
        messagingTemplate.convertAndSendToUser(
                lobbyId,"/queue/events",
                new Event(
                        EventType.ADD_USER,
                        user.getId(),
                        lobbyId
                )
        );
    }

    @GetMapping("/{id}")
    public LobbyDTO getGame(@PathVariable String id) {
        return new LobbyDTO(lobbyService.findLobbyById(id).get());
    }

    @GetMapping("/")
    public List<LobbyDTO> findAll() {
        return lobbyService.findAll().stream().filter(Objects::nonNull).map(LobbyDTO::new).toList();
    }

    @PostMapping("/test")
    public String savePack(@RequestBody Pack pack) {
        if (Objects.isNull(pack.getId())) {
            pack.setId(UUID.randomUUID().toString());
        }
        return pack.getId();
    }
}
