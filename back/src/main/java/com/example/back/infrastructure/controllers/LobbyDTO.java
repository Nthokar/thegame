package com.example.back.infrastructure.controllers;

import com.example.back.domain.game.Lobby;
import com.example.back.domain.game.LobbyState;
import com.example.back.domain.game.Master;
import com.example.back.domain.game.Player;
import com.example.back.domain.game.pack.Pack;

import java.util.List;

public class LobbyDTO {
    public String id;
    public Pack pack;

    public List<Player> players;
    public Master master;

    public LobbyDTO(Lobby lobby) {
        this.id = lobby.getId();
        this.pack = lobby.getPack();
        this.players = lobby.getPlayers();
        this.master = lobby.getMaster();
    }
}
