package com.example.back.infrastructure.controllers;

import com.example.back.app.services.LobbyService;
import com.example.back.domain.User;
import com.example.back.domain.game.Lobby;
import com.example.back.domain.game.LotDTO;
import com.example.back.domain.game.Player;
import com.example.back.domain.game.events.*;
import com.example.back.domain.game.pack.Pack;
import com.example.back.domain.game.pack.Round;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.Objects;

@Controller
@Slf4j
public class SocketLobbyController {

    private final SimpMessagingTemplate messagingTemplate;
    private final LobbyService lobbyService;
    private static final String QUEUE = "/queue/events";
    public SocketLobbyController(SimpMessagingTemplate messagingTemplate,LobbyService lobbyService){
        this.lobbyService=lobbyService;
        this.messagingTemplate=messagingTemplate;
    }

   /*
    *public void getRound(){}
    *public void setPicker(){}
    *public void getLot(){}
    *    после выбора отправка во вне
    *
    *public void setScore(){}
    *public void resheniyeVedushchego
    */
    @MessageMapping("/lobby/{lobbyId}/getRound")
    public void getRound(@DestinationVariable("lobbyId") String lobbyId){
        Round round = lobbyService.getCurRound(lobbyId);
        messagingTemplate.convertAndSend(
            "/topic/lobby/" + lobbyId + QUEUE, round
        );
    }

    @MessageMapping("/lobby/{lobbyId}/pickLot")
    public void pickLot(@DestinationVariable("lobbyId") String lobbyId, @Payload LotDTO lotDTO) {
        var events = lobbyService.pickLot(lobbyId, lotDTO);
        if (Objects.nonNull(events)) {
            for (int i = 0; i < events.length; i++){
                messagingTemplate.convertAndSend(
                        "/topic/lobby/" + lobbyId + QUEUE, events[i]);
            }
        }
    }

    @MessageMapping("/lobby/{lobbyId}/setPicker")
    public void setPicker(@DestinationVariable("lobbyId") String lobbyId,@Payload String pickerId){
        Event[] resp = new Event[0];

        try {
            //pickerId na master pomenyat
            Player picker = lobbyService.setPicker(lobbyId, pickerId);
            resp = new Event[]{new PickerEvent(picker)};
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
        messagingTemplate.convertAndSendToUser(
                lobbyId, QUEUE, resp
        );
    }

    @MessageMapping("/lobby/{lobbyId}/addUser")
    public void addUser(@DestinationVariable("lobbyId") String lobbyId, User user){
        Player player = lobbyService.addUser(lobbyId, user);
        messagingTemplate.convertAndSendToUser(
                lobbyId, QUEUE,
                new NewUserEvent(player));
    }
/*    @MessageMapping("/lobby/leave")
    public void leaveLobby(@Payload Event event){
        Event resp;
        try{

        }
        catch (Exception e){

        }
    }*/

    @MessageMapping("/lobby/{lobbyId}/start")
    public void startGame(@DestinationVariable("lobbyId") String lobbyId){
        Event[] resp = new Event[0];
        try{
            resp = lobbyService.start(lobbyId);
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
        for (int i = 0; i < resp.length; i++){
            messagingTemplate.convertAndSend(
                    "/topic/lobby/" + lobbyId + QUEUE, resp[i]);
        }
    }

    @MessageMapping("/lobby/{lobbyId}/set_pack")
    public void setPack(@DestinationVariable("lobbyId") String lobbyId, Pack pack) {
        Event[] resp = new Event[0];
        try {
            lobbyService.setPack(lobbyId, pack);
            resp = new Event[] { new SetPackEvent(pack) };
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        for (int i = 0; i < resp.length; i++){
            messagingTemplate.convertAndSend(
                    "/topic/lobby/" + lobbyId + QUEUE, resp[i]);
        }
    }


    @MessageMapping("/lobby/{lobbyId}/pause_game")
    public void pauseGame(@DestinationVariable("lobbyId") String lobbyId){
        Event[] resp = new Event[0];

        try {
            resp = lobbyService.pauseGame(lobbyId);
        }
        catch (Exception e) {
            log.error(e.getMessage());
        }
        for (int i = 0; i < resp.length; i++){
            messagingTemplate.convertAndSend(
                    "/topic/lobby/" + lobbyId + QUEUE, resp[i]);
        }
    }

    @MessageMapping("/lobby/{lobbyId}/resume_game")
    public void resumeGame(@DestinationVariable("lobbyId") String lobbyId){
        Event[] resp = null;
        try {
            resp = lobbyService.resumeGame(lobbyId);
        }
        catch (Exception e) {
            log.error(e.getMessage());
        }
        for (int i = 0; i < resp.length; i++){
            messagingTemplate.convertAndSend(
                    "/topic/lobby/" + lobbyId + QUEUE, resp[i]);
        }
    }
    @MessageMapping("/lobby/{lobbyId}/ping")
    public void pingPong(@DestinationVariable("lobbyId") String lobbyId, UserIdDto userId) {
        var events = lobbyService.tryPingPong(lobbyId, userId.userId);
        if (Objects.nonNull(events)) {
            for (int i = 0; i < events.length; i++){
                messagingTemplate.convertAndSend(
                        "/topic/lobby/" + lobbyId + QUEUE, events[i]);
            }
        }
    }
    record UserIdDto(String userId) {}
    @MessageMapping("/lobby/{lobbyId}/score_update")
    public void scoreUpdate(@DestinationVariable("lobbyId") String lobbyId, String userId, String targetId, int delta) {
        Lobby lobby = lobbyService.findLobbyByIdOrThrow(lobbyId);
        if (lobby.getMaster().getUser().getId().equals(userId)) {
            var events = lobbyService.scoreUpdate(lobbyId, targetId, delta);
            for (int i = 0; i < events.length; i++){
                messagingTemplate.convertAndSend(
                        "/topic/lobby/" + lobbyId + QUEUE, events[i]);
            }
        }
    }

    @MessageMapping("/lobby/{lobbyId}/answer_verification")
    public void answerVerification(@DestinationVariable("lobbyId") String lobbyId, AnswerDto answerDto) {
        Lobby lobby = lobbyService.findLobbyByIdOrThrow(lobbyId);
        if (lobby.getMaster().getUser().getId().equals(answerDto.userId)) {
            var events = lobbyService.answerVerification(lobbyId, answerDto.isCorrect);
            for (int i = 0; i < events.length; i++){
                messagingTemplate.convertAndSend(
                        "/topic/lobby/" + lobbyId + QUEUE, events[i]);
            }
        }
    }
    record AnswerDto(String userId, boolean isCorrect) {}
}
