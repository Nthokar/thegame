package com.example.pack.repos;

import com.example.pack.domain.pack.Pack;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PackRepository extends MongoRepository<Pack, String> {
}
