package com.example.pack.domain.pack;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
@NoArgsConstructor
@Document
@Data
public class Round implements Serializable {
    List<Category> categories;

    public Round(List<Category> categories) {
        this.categories = categories;
    }
}
