package com.example.pack.domain.pack;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Document
@Data
public class Question {
    Content content;
    String answer;

    public Question(Content content, String answer) {
        this.content = content;
        this.answer = answer;
    }
}
