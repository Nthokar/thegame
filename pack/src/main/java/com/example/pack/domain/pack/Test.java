package com.example.pack.domain.pack;

import com.example.pack.domain.pack.lots.Lot;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Document
@Data
public class Test {
    @Id
    String id;

    HashMap<String, List<Lot>> lotsByCategory;
}
