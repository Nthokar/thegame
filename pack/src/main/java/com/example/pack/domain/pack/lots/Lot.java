package com.example.pack.domain.pack.lots;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = QuestionLot.class, name = "QuestionLot"),
})
public abstract class Lot {
    @Getter
    Integer reward;
    String type;

    Lot(Integer reward, String type) {
        this.reward = reward;
        this.type = type;
    }
}
