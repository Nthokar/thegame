package com.example.pack.domain.pack.lots;

import com.example.pack.domain.pack.Question;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@JsonTypeName("QuestionLot")
@ToString(onlyExplicitlyIncluded = true)
@Document
@Data
public class QuestionLot extends Lot {
    Question question;

    public QuestionLot(Question question, Integer reward) {
        super(reward, QuestionLot.class.getTypeName());
        this.question = question;
    }
}
