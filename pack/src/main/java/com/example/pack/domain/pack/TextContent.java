package com.example.pack.domain.pack;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Data
public class TextContent extends Content {
    String content;

    public TextContent(String content) {
        super(TextContent.class.getTypeName());
        this.content = content;
    }
}
