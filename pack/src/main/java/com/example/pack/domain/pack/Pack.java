package com.example.pack.domain.pack;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Document
@Data
public class Pack implements Serializable {
    @Id
    @ToString.Include
    String id;
    List<Round> rounds;

    public Pack(List<Round> rounds) {
        this.id = String.valueOf(UUID.randomUUID());
        this.rounds = rounds;
    }

    public Pack(List<Round> rounds, String id) {
        this.id = id;
        this.rounds = rounds;
    }
}