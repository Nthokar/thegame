package com.example.pack.extern.controllers;

import com.example.pack.domain.pack.*;
import com.example.pack.repos.PackRepository;
import com.example.pack.repos.TestRepo;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.UUID;

@RestController
public class PackController {
    private final PackRepository packRepository;
    private final TestRepo testRepo;

    public PackController(PackRepository packRepository, TestRepo testRepo) {
        this.packRepository = packRepository;
        this.testRepo = testRepo;
    }

    @GetMapping("/")
    public Pack getPack(@RequestParam String packId) {
        Pack pack = packRepository.findById(packId).orElseThrow(RuntimeException::new);
        return pack;
    }

    @PostMapping
    public String savePack(@RequestBody Pack pack) {
        if (Objects.isNull(pack.getId())) {
            pack.setId(UUID.randomUUID().toString());
        }
        packRepository.save(pack);
        return pack.getId();
    }

    @GetMapping("/test")
    public Test getTest(@RequestParam String testId) {
        Test test = testRepo.findById(testId).orElseThrow(RuntimeException::new);
        return test;
    }

    @PostMapping("/test")
    public String saveTest(@RequestBody Test test) {
        if (Objects.isNull(test.getId())) {
            test.setId(UUID.randomUUID().toString());
        }
        testRepo.save(test);
        return test.getId();
    }
}
